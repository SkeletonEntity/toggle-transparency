;;; toggle-transparency.el --- Provides a function to toggle transparency as well as lower and raise it by increments.                    -*- lexical-binding: t; -*-

;; Copyright (C) 2021 SkeletonAdventure

;; Author: SkeletonAdventure <skeletonadventure@protonmail.com>
;; Keywords: toggle, transparency, interactive
;; Version: 1.0

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, with version 3 of the License, and
;; no other version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides a function to toggle transparency as well as lower and raise it by increments.

;;; Code:
;; (set-frame-parameter (selected-frame) 'alpha '(90 . 90))

(defgroup toggle-transparency nil
  "Provides a function to toggle transparency as well as lower and raise it by increments."
  :group 'tools)

(defcustom toggle-transparency-save-file "~/.emacs.d/var/toggle-transparency/save.txt"
  "The last set transparency is stored in this file, accessed on frame creation."
  :type 'string
  :group 'toggle-transparency)

(defcustom toggle-transparency-step-amount 5
  "The amount to step for each raise or lower in transparency."
  :type 'number
  :group 'toggle-transparency)

(defun read-lines (filePath)
  "Return a list of lines of a file at filePath."
  (with-temp-buffer
    (insert-file-contents filePath)
    (split-string (buffer-string) "\n" t)))

(defun write-alpha (active inactive)
  "Writes the active and inactive values on separate lines in the
toggle-transparency-save-file."
  (write-region (format "%d\n%d" active inactive) nil toggle-transparency-save-file))

(defun toggle-transparency-save (active inactive)
  "Writes the active and inactive values on separate lines in the
toggle-transparency-save-file, creates the file if it doesn't exist."
  (unless (file-exists-p toggle-transparency-save-file)
    (make-empty-file toggle-transparency-save-file t))
  (write-alpha active inactive))

(defun toggle-transparency-save-active (active)
  "Writes the first line, the active value, in the toggle-transparency-save-file,
 creates the file if it doesn't exist."
  (if (file-exists-p toggle-transparency-save-file)
      (let* ((lines (read-lines toggle-transparency-save-file))
	     (inactive (string-to-number (nth 1 lines))))
        (write-alpha active inactive))
    (make-empty-file toggle-transparency-save-file)
    (write-alpha active 100)))

(defun toggle-transparency-save-inactive (inactive)
  "Writes the second line, the inactive value, in the toggle-transparency-save-file,
 creates the file if it doesn't exist."
  (if (file-exists-p toggle-transparency-save-file)
      (let* ((lines (read-lines toggle-transparency-save-file))
	     (active (string-to-number (nth 0 lines))))
	(write-alpha active inactive))
    (make-empty-file toggle-transparency-save-file)
    (write-alpha 100 inactive)))

(defun toggle-transparency-load ()
  "Loads the alpha values from toggle-transparency-save-file, if the
files does not exist it creates it with the default of (100 . 100)"
  (unless (file-exists-p toggle-transparency-save-file)
    (toggle-transparency-save 100 100))
  (let* ((lines (read-lines toggle-transparency-save-file))
	 (active (string-to-number (nth 0 lines)))
	 (inactive (string-to-number (nth 1 lines))))
    (cons active inactive)))

(defun toggle-transparency ()
  "Toggles between the saved transparency value and opaque."
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         (toggle-transparency-load) '(100 . 100)))))

(delete '(alpha . (75 . 75)) default-frame-alist)
default-frame-alist

(defun toggle-transparency-reload ()
  "Reloads the saved transparency value, setting the parameter and changing the
default-frame-alist."
  (interactive)
  (let ((loaded (toggle-transparency-load)))
    (setq default-frame-alist (delete (cons 'alpha (frame-parameter nil 'alpha)) default-frame-alist))
    (add-to-list 'default-frame-alist (cons 'alpha loaded))
    (set-frame-parameter (selected-frame) 'alpha loaded)))
  
(defun toggle-transparency-set-active (x)
  "Sets the active transparency and saves the change."
  (interactive "nEnter active alpha: ")
  (toggle-transparency-save-active x)
  (toggle-transparency-reload))

(defun toggle-transparency-set-inactive (x)
  "Sets the inactive transparency and saves the change."
  (interactive "nEnter inactive alpha: ")
  (toggle-transparency-save-inactive x)
  (toggle-transparency-reload))

(defun toggle-transparency-change-alpha (x)
  "Changes the saved transparency by x and then reloads the transparency."
  (let* ((loaded (toggle-transparency-load))
	 (active (clamp100 (+ x (car loaded))))
	 (inactive (clamp100 (+ x (car (last loaded))))))
      (toggle-transparency-save active inactive)
      (toggle-transparency-reload)
      (message (format "Set transparency to '(%d . %d)" active inactive))))

(defun clamp100 (x)
  "Clamps the given integer to be in [0,100]."
  (min (max x 0) 100))

(defun toggle-transparency-raise-alpha ()
  "Raises the transparency by the toggle-transparency-step-amount."
  (interactive)
  (toggle-transparency-change-alpha toggle-transparency-step-amount))

(defun toggle-transparency-lower-alpha ()
  "Lowers the transparency by the toggle-transparency-step-amount."
  (interactive)
  (toggle-transparency-change-alpha (- toggle-transparency-step-amount)))

(provide 'toggle-transparency)

;;; toggle-transparency.el ends here
